<!DOCTYPE html>
<html lang="en">

<?php include 'includes/header.php' ?>

<body>

    <?php include 'includes/topbar.php' ?>

    <?php include 'includes/navigation.php' ?>

    <!-- Header Start -->
    <div class="container-fluid page-header" style="margin-bottom: 90px;">
        <div class="container">
            <div class="d-flex flex-column justify-content-center" style="min-height: 300px">
                <h3 class="display-4 text-white text-uppercase">Tentang Kami</h3>
                <div class="d-inline-flex text-white">
                    <p class="m-0 text-uppercase"><a class="text-white" href="">Laman Utama</a></p>
                    <i class="fa fa-angle-double-right pt-1 px-3"></i>
                    <p class="m-0 text-uppercase">Tentang Kami</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Header End -->

    <!-- About Start -->
    <div class="container-fluid py-5">
        <div class="container py-5">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <img class="img-fluid rounded mb-4 mb-lg-0" src="img/syarikat-min.jpg" alt="">
                </div>
                <div class="col-lg-6">
                    <div class="text-left mb-4">
                        <h5 class="text-primary text-uppercase mb-3" style="letter-spacing: 5px;">Tentang Kami</h5>
                        <h1>Elegance Global Trading Sdn Bhd</h1>
                    </div>
                    <p class="text-uppercase">Berdaftar pada 29 September 2014</p>
                    <p class="text-justify">Jutawan.elegancegroup.my adalah dibawah naungan Elegance Global Trading Sdn Bhd, menyediakan peluang rakanniaga yang mampu melahirkan usahawan-usahawan
                        yang berpotensi, berkaliber bijak mengendalikan sistem E-dagang untuk merujuk kepada jualan produk fizikal secara dalam talian, tetapi ia juga boleh menerangkan
                        apa-apa jenis transaksi komersial yang difasilitasi melalui internet.
                    </p>
                    <p class="text-justify">COVID 19: Elegance Global teroka strategi baharu peralihan trend yang mendorong pengguna untuk meneroka strategi baharu bagi memasarkan produk, supaya membantu
                        kepada yang kehilangan pekerjaan atau perniagaan agar mampu memperolehi pendapatan yang sangat baik untuk mereka yang serius bersama kami.
                    </p>
                    <p class="text-justify">Syarikat juga memberi tunjuk ajar mengendalikan sistem yang mudah terkendali, serta kursus-kursus juga disediakan untuk meningkatkan menjadi Usahawan yang Berjaya
                        dimasa akan datang.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid bg-registration py-3" style="margin: 10px 0;">
        <div class="container py-5">
            <div class="row align-items-center">
                <div class="col-lg-6 mb-5 mb-lg-0">
                    <div>
                        <div class="text-center p-4">
                            <h3 class="m-0 text-primary text-uppercase" style="letter-spacing: 5px;">
                                <i class="fa fa-eye mr-3"></i>Visi
                            </h3>
                        </div>
                        <div class="card-body rounded-bottom p-3">
                            <h5 class="text-white mb-3 text-center">Untuk menyediakan platform perniagaan bertaraf dunia bagi usahawan ke arah kejayaan bersama di seluruh dunia</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div>
                        <div class="text-center p-4">
                            <h3 class="m-0 text-primary text-uppercase" style="letter-spacing: 5px;">
                                <i class="fa fa-paper-plane mr-3"></i>Misi
                            </h3>
                        </div>
                        <div class="card-body rounded-bottom p-3">
                            <h5 class="text-white mb-3 text-center">Untuk menyediakan platform perniagaan bertaraf dunia bagi usahawan ke arah kejayaan bersama di seluruh dunia</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About End -->

    <!-- Team Start -->
    <div class="container-fluid py-5">
        <div class="container pt-5 pb-3">
            <div class="text-center mb-5">
                <h5 class="text-primary text-uppercase mb-3" style="letter-spacing: 5px;">Organisasi</h5>
                <h1>Struktur Organisasi Syarikat</h1>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-3 text-center team mb-4">
                    <div class="team-item rounded overflow-hidden mb-2">
                        <div class="team-img position-relative">
                            <img class="img-fluid" src="img/team-1.jpg" alt="">
                            <div class="team-social">
                                <a class="btn btn-outline-light btn-square mx-1" href="#" data-toggle="modal" data-target="#modal-chairman"><i class="fas fa-user"></i></a>
                            </div>
                        </div>
                        <div class="bg-secondary p-4">
                            <h5>Datin DR. Shamsiah Binti Yaakob</h5>
                            <p class="m-0">Executive Chairman</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 text-center team mb-4">
                    <div class="team-item rounded overflow-hidden mb-2">
                        <div class="team-img position-relative">
                            <img class="img-fluid" src="img/team-2.jpg" alt="">
                            <div class="team-social">
                                <a class="btn btn-outline-light btn-square mx-1" href="#" data-toggle="modal" data-target="#modal-director"><i class="fas fa-user"></i></a>
                            </div>
                        </div>
                        <div class="bg-secondary p-4">
                            <h5>Andi Saputra Bin Sukaman</h5>
                            <p class="m-0">Managing Director</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 text-center team mb-4">
                    <div class="team-item rounded overflow-hidden mb-2">
                        <div class="team-img position-relative">
                            <img class="img-fluid" src="img/team-3.jpg" alt="">
                            <div class="team-social">
                                <a class="btn btn-outline-light btn-square mx-1" href="#" data-toggle="modal" data-target="#modal-executive"><i class="fas fa-user"></i></a>
                            </div>
                        </div>
                        <div class="bg-secondary p-4">
                            <h5>Datok Salim Bin Rohani</h5>
                            <p class="m-0">Chief Executive Officer</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 text-center team mb-4">
                    <div class="team-item rounded overflow-hidden mb-2">
                        <div class="team-img position-relative">
                            <img class="img-fluid" src="img/team-4.jpg" alt="">
                            <div class="team-social">
                                <a class="btn btn-outline-light btn-square mx-1" href="#" data-toggle="modal" data-target="#modal-id"><i class="fas fa-user"></i></a>
                            </div>
                        </div>
                        <div class="bg-secondary p-4">
                            <h5>En Mohd Naufal Bin Abdul Latiff</h5>
                            <p class="m-0">Marketing Manager</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for Executive Chairman Profile Start -->
    <div id="modal-chairman" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="text-center"><img src="img/team-1.jpg"></div>
                        </div>
                        <div class="col-12">
                            <h2 class="px-3 pt-4 pb-1">Datin DR. Shamsiah Binti Yaakob</h2>
                            <h5 class="px-3 pb-4">Executive Chairman</h5>
                            <p class="text-justify px-3">
                                Datin Shamsiah Yaakob adalah seorang usahawan yang telah aktif sejak tahun 1999 yang merupakan pemilik ELEGANCE GROUP, dia adalah seorang pelatih usahawan
                                yang berpengalaman selama 10 tahun, mempunyai sifat kepimpinan yang tinggi dalam mendidik generasi baru dalam bidang keusahawanan.
                            </p>
                            <p class="text-justify px-3">
                                Dia telah melahirkan 200 generasi baru dalam perniagaan di Malaysia dan Indonesia. Beliau memiliki jurusan Sarjana Pentadbiran dalam Pemasaran dan juga Master
                                dalam Industri HALAL. Dia adalah jawatankuasa Halal JAKIM Malaysia. Dia juga aktif dalam NGO NEXTGENT sebagai naib presiden. Dia juga telah menerima Anugerah Anggota Negara (ANS).
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal for Executive Chairman Profile End -->

    <!-- Modal for Managing Director Profile Start -->
    <div id="modal-director" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="text-center"><img src="img/team-2.jpg"></div>
                        </div>
                        <div class="col-12">
                            <h2 class="px-3 pt-4 pb-1">Andi Saputra Bin Sukaman</h2>
                            <h5 class="px-3 pb-4">Managing Director</h5>
                            <p class="text-justify px-3">
                                Andi Saputra bin Sukaman mempunyai pengalaman dalam bidang keusahawanan selama lebih dari 20 tahun. Dia lahir dari Indonesia dan dia juga merupakan motivator yang sangat baik dan
                                dia juga merupakan motivator yang sangat baik dan telah memotivasi generasi muda untuk tidak bergantung kepada kerjaya mereka pada pemerintah.
                            </p>
                            <p class="text-justify px-3">
                                Dia adalah pakar dalam teknologi maklumat dan juga harta tanah. Dia melakukan kerja perundingan untuk beberapa syarikat nasional dan mengasaskan persatuan untuk pemaju harta tanah di Indonesia.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal for Managing Director Profile End -->

    <!-- Modal for Chief Executive Profile Start -->
    <div id="modal-executive" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="text-center"><img src="img/team-3.jpg"></div>
                        </div>
                        <div class="col-12">
                            <h2 class="px-3 pt-4 pb-1">Datok Salim Bin Rohani</h2>
                            <h5 class="px-3 pb-4">Chief Executive Officer</h5>
                            <p class="text-justify px-3">
                                Dato' Salim Bin Rohani berpengalaman dalam pembangunan koperat serta membantu melahirkan usahawan baru juga seorang speker bagi beberapa syarikat koperat dan networking.
                            </p>
                            <p class="text-justify px-3">
                                Juga seorang penasihat kewangan bagi membantu usahawan, koperasi serta syarikat-syarikat koperat untuk mengembangkan perniagaan.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal for Chief Executive Profile End -->

    <!-- Modal for Naufal Firasat Profile Start -->
    <div id="modal-id" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="text-center"><img src="img/team-4.jpg"></div>
                        </div>
                        <div class="col-12">
                            <h2 class="px-3 pt-4 pb-1">En Mohd Naufal Bin Abdul Latiff</h2>
                            <h5 class="px-3 pb-4">Marketing Manager</h5>
                            <p class="text-justify px-3">Mempunyai Diploma Life Insurance di Malaysian Insurance Institute.</p>
                            <p class="text-justify px-3">Bekas pengurus jualan automobil, insurance dan produk-produk tempatan.</p>
                            <p class="text-justify px-3">Pernah dianugerahkan Top Salesman Proton disalah sebuah outlet Hicom Mobil.</p>
                            <p class="text-justify px-3">Head of Marketing disalah sebuah syarikat makanan.</p>
                            <p class="text-justify px-3">Merupakan seorang speaker usahawan, DJ, MC, Penyanyi, Event Organizer, Buskers dan Artis Sambilan.</p>
                            <p class="text-justify px-3">Bekas Ahli Jawatankuasa Gerak Tenaga Ekonomi Negara (GTEN) anjuran MARA.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal for Naufal Firasat Profile End -->


    <?php include 'includes/footer.php' ?>

    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="fa fa-angle-double-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>

    <!-- Contact Javascript File -->
    <script src="mail/jqBootstrapValidation.min.js"></script>
    <script src="mail/contact.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
</body>

</html>