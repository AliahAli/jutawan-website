<!DOCTYPE html>
<html lang="en">

<?php include 'includes/header.php' ?>


<body>
    <?php include 'includes/topbar.php' ?>

    <?php include 'includes/navigation.php' ?>

    <!-- Carousel Start -->
    <div class="container-fluid mb-3 border-top pt-3">
        <div class="row">
            <div class="col-lg-8">
                <video controls width="100%" autoplay muted playsinline>
                    <source src="video/video.mp4" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
            </div>
            <!-- Mini Control Panel Start -->
            <div class="col-lg-4">
                <div class="container bg-white shadow rounded p-4">
                    <div class="row">
                        <div class="col-lg-6 col-md-2">
                            <div class="overflow-hidden mb-2">
                                <div class="p-2 text-center">
                                    <a href=""><i class="fas fa-2x fa-shopping-basket text-primary bg-white rounded shadow p-2"></i></a>
                                    <div class="text-center mt-1">
                                        <small>Semak Transaksi</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-2">
                            <div class="overflow-hidden mb-2">
                                <div class="p-2 text-center">
                                    <a href=""><i class="fas fa-2x fa-sitemap text-primary bg-white rounded shadow p-2"></i></a>
                                    <div class="text-center mt-1">
                                        <small>Info Jaringan</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-2">
                            <div class="overflow-hidden mb-2">
                                <div class="p-2 text-center">
                                    <a href=""><i class="fas fa-2x fa-percent text-primary bg-white rounded shadow p-2"></i></a>
                                    <div class="text-center mt-1">
                                        <small>Bonus</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-2">
                            <div class="overflow-hidden mb-2">
                                <div class="p-2 text-center">
                                    <a href=""><i class="fas fa-2x fa-wallet text-primary bg-white rounded shadow p-2"></i></a>
                                    <div class="text-center mt-1">
                                        <small>E-Wallet</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-2">
                            <div class="overflow-hidden mb-2">
                                <div class="p-2 text-center">
                                    <a href=""><i class="fas fa-2x fa-shopping-cart text-primary bg-white rounded shadow p-2"></i></a>
                                    <div class="text-center mt-1">
                                        <small>Troli</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-2">
                            <div class="overflow-hidden mb-2">
                                <div class="p-2 text-center">
                                    <a href=""><i class="fas fa-2x fa-users text-primary bg-white rounded shadow p-2"></i></a>
                                    <div class="text-center mt-1">
                                        <small>Profil</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-2">
                            <div class="overflow-hidden mb-2">
                                <div class="p-2 text-center">
                                    <a href=""><i class="fas fa-2x fa-balance-scale text-primary bg-white rounded shadow p-2"></i></a>
                                    <div class="text-center mt-1">
                                        <small>Kod Etika</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-2">
                            <div class="overflow-hidden mb-2">
                                <div class="p-2 text-center">
                                    <a href=""><i class="fas fa-2x fa-user-plus text-primary bg-white rounded shadow p-2"></i></a>
                                    <div class="text-center mt-1">
                                        <small>Promosi Naik Taraf Ahli</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mini Control Panel End -->
        </div>
    </div>
    <!-- Carousel End -->


    <!-- About Start -->
    <div class="container-fluid py-5">
        <div class="container py-5">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <img class="img-fluid rounded mb-4 mb-lg-0" src="img/product/logo.png" alt="">
                </div>
                <div class="col-lg-7">
                    <div class="text-left mb-4">
                        <h5 class="text-primary text-uppercase mb-3" style="letter-spacing: 5px;">Bismillahirrahmanirrahim</h5>
                        <h1>Sertai Kami Sekarang</h1>
                    </div>
                    <p>Rebut Peluang Menjadi Usahawan Berjaya</p>
                    <p>Sertai JUTAWANELEGANCE4U.com. membina rangkaian seterusnya memaksimakan pendapatan anda.</p>
                    <p>Daftar SEGERA hari ini dan kami akan terangkan dengan LEBIH LANJUT dan BANTU ANDA untuk BERJAYA BERSAMA KAMI.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- About End -->


    <!-- Registration Start -->
    <div class="container-fluid bg-registration py-5" style="margin: 90px 0;">
        <div class="container py-5">
            <div class="row align-items-center">
                <div class="col-lg-7 mb-5 mb-lg-0">
                    <div class="mb-4">
                        <h5 class="text-primary text-uppercase mb-3" style="letter-spacing: 5px;">ELEGANCE GLOBAL TRADING SDN BHD</h5>
                        <h1 class="text-white">ROADMAP</h1>
                    </div>
                    <p class="text-white">1. Trading 2014</p>
                    <ul class="list-inline text-white m-0">
                        <li class="py-2"><i class="fa fa-check text-primary mr-3"></i>Pengenalan produk halal</li>
                        <li class="py-2"><i class="fa fa-check text-primary mr-3"></i>Jualan produk di Malaysia dan Indonesia</li>
                    </ul>

                    <div class="pt-4">
                        <p class="text-white">2. Gabung badan-badan swasta/kerajaan/NGO</p>
                        <ul class="list-inline text-white m-0">
                            <li class="py-2"><i class="fa fa-check text-primary mr-3"></i>Gabungan usahasama badan-badan swasta/kerajaan/NGO bagi mencapai misi menyediakan platform perniagaan bertaraf antarabangsa untuk semua golongan supaya celik IT dalam dunia e-dagang</li>
                        </ul>
                    </div>
                    <div class="pt-4">
                        <p class="text-white">3. Trading Networking 2021 Jutawan.elegancegroup.my</p>
                        <ul class="list-inline text-white m-0">
                            <li class="py-2"><i class="fa fa-check text-primary mr-3"></i>Memperluaskan komuniti usahawan seluruh dunia.</li>
                            <li class="py-2"><i class="fa fa-check text-primary mr-3"></i>Pengenalan produk halal tanpa kandungan terlarang kepada semua peringkat usia, bagi pencegahan penyakit serta pemulihan penyakit mahupun membina kesihatan yang lebih sihat kepada individu atau seisi keluarga.</li>

                        </ul>
                    </div>

                </div>
                <div class="col-lg-5">
                    <ul class="list-inline text-white m-0">
                        <li class="py-2"><i class="fa fa-check text-primary mr-3"></i>Pengenalan produk halal tanpa kandungan terlarang kepada semua peringkat usia, bagi pencegahan penyakit serta pemulihan penyakit mahupun membina kesihatan yang lebih sihat kepada individu atau seisi keluarga.</li>
                        <li class="py-2"><i class="fa fa-check text-primary mr-3"></i>Menyediakan platform sistem perniagaan online dan offline.</li>
                        <li class="py-2"><i class="fa fa-check text-primary mr-3"></i>Menyediakan imbuhan-imbuhan atau insentif berbentuk pendapatan tambahan menampung sara hidup secara part time atau full time dalam keadaan melandanya Covid-19</li>
                        <li class="py-2"><i class="fa fa-check text-primary mr-3"></i>Sesi pengenalan atau ceramah di mana-mana daerah atau negeri.</li>
                        <li class="py-2"><i class="fa fa-check text-primary mr-3"></i>Kursus-kursus disediakan bagi menaik taraf keupayaan diri dari orang biasa menjadi usahawan serta jutawan berjaya.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container pt-5 pb-3">
        <div class="row">
            <div class="col-12">
                <div class="d-flex justify-content-center">
                    <video controls width="100%">
                        <source src="video/video2.mp4" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                </div>
            </div>
        </div>
    </div>
    <!-- Registration End -->

    <div class="container-fluid py-5">
        <div class="container py-5">
            <div class="row align-items-center">
                <div class="col-lg-7 mb-5 mb-lg-0">
                    <div class="mb-4">
                        <h1 class="m-0">KERJA BERSAMA BERJAYA BERSAMA</h1>
                    </div>
                    <p class="m-0">Sila lengkapkan borang berikut:</p>
                </div>
                <div class="col-lg-5">
                    <div class="card border-0">
                        <div class="card-header bg-light text-center p-4">
                            <h1 class="m-0">Daftar sekarang !</h1>
                        </div>
                        <div class="card-body rounded-bottom bg-primary p-5">
                            <form>
                                <div class="form-group">
                                    <input type="text" class="form-control border-0 p-4" placeholder="Username" required="required" />
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control border-0 p-4" placeholder="Password Login" required="required" />
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control border-0 p-4" placeholder="Full Name" required="required" />
                                </div>
                                <div class="form-group">
                                    <select class="custom-select border-0 px-4" style="height: 47px;">
                                        <option selected>State</option>
                                        <option value="Johor">Johor</option>
                                        <option value="Kedah">Kedah</option>
                                        <option value="Kelantan">Kelantan</option>
                                        <option value="Kuala Lumpur">Kuala Lumpur</option>
                                        <option value="Melaka">Melaka</option>
                                        <option value="Negeri Sembilan">Negeri Sembilan</option>
                                        <option value="Pahang">Pahang</option>
                                        <option value="Perak">Perak</option>
                                        <option value="Perlis">Perlis</option>
                                        <option value="Pulau Pinang">Pulau Pinang</option>
                                        <option value="Sabah">Sabah</option>
                                        <option value="Sarawak">Sarawak</option>
                                        <option value="Selangor">Selangor</option>
                                        <option value="Terengganu">Terengganu</option>
                                        <option value="Brunei">Brunei</option>
                                        <option value="Indonesia">Indonesia</option>
                                        <option value="Singapore">Singapore</option>
                                        <option value="Thailand">Thailand</option>
                                        <option value="Lain-lain">Lain-lain</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control border-0 p-4" placeholder="Phone Number" required="required" />
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control border-0 p-4" placeholder="Email" required="required" />
                                </div>
                                <div>
                                    <button class="btn btn-dark btn-block border-0 py-3" type="submit">Register</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php include 'includes/footer.php' ?>

    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="fa fa-angle-double-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>

    <!-- Contact Javascript File -->
    <script src="mail/jqBootstrapValidation.min.js"></script>
    <script src="mail/contact.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
</body>

</html>
