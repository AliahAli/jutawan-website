<!-- Navbar Start -->
<div class="container-fluid">
    <div class="row border-top px-xl-5">
        <div class="col-xs-6 col-sm-6 col-lg-12 col-xl-12">
            <nav class="navbar navbar-expand-lg bg-light navbar-light py-3 py-lg-0 px-0">
                <a href="" class="text-decoration-none d-block d-lg-none">
                    <h1 class="m-0"><span class="text-primary">JUTAWAN</span>ELEGANCE</h1>
                </a>
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                    <div class="navbar-nav nav justify-content-center py-0">
                        <a href="index" class="nav-item nav-link">Laman Utama</a>
                        <a href="product" class="nav-item nav-link">Produk</a>
                        <a href="pelan" class="nav-item nav-link">Pelan</a>
                        <a href="testimoni" class="nav-item nav-link">Testimoni</a>
                        <a href="stokis" class="nav-item nav-link">Stokis</a>
                        <a href="#" class="nav-item nav-link">Jualan dan Bonus</a>
                        <a href="#" class="nav-item nav-link">Pendaftaran Pengedar</a>
                        <a href="#" class="nav-item nav-link">Landing Page Recruit</a>
                        <a href="#" class="nav-item nav-link">Promo Info</a>
                        <a href="#" class="nav-item nav-link">Muat Turun</a>
                        <a href="#" class="nav-item nav-link">Cabutan Bertuah</a>
                        <a href="#" class="nav-item nav-link">Promo Upgrade Member</a>
                        <div class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Akaun</a>
                            <div class="dropdown-menu rounded-0 m-0">
                                <a href="https://jutawanelegance.com/member/login.php" target="_blank" class="dropdown-item">Login</a>
                                <a href="" class="dropdown-item">Daftar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
<!-- Navbar End -->