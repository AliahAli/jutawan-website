<!-- Footer Start -->
<div class="container-fluid bg-dark text-white py-5 px-sm-3 px-lg-5" style="margin-top: 90px;">
    <div class="row pt-5">
        <div class="col-lg-7 col-md-12">
            <div class="row">
                <div class="col-md-6 mb-5">
                    <h5 class="text-primary text-uppercase mb-4" style="letter-spacing: 5px;">Hubungi Kami</h5>
                    <p><i class="fa fa-map-marker-alt mr-2"></i><a style="text-decoration: none; color: white;" href="https://goo.gl/maps/osow6fYbWA9X7qvAA" target="_blank">
                            42-1, Jalan Komersil Senawang 7, Taman Komersil Senawang, 70450 Seremban, Negeri Sembilan</a></p>
                    <p><i class="fa fa-phone-alt mr-2"></i><a style="text-decoration: none; color: white;" href="tel: 0196388448">+019 638 8448</a></p>
                    <p><i class="fa fa-envelope mr-2"></i><a style="text-decoration: none; color: white;" href="mailto: azizigandan@ymail.com">azizigandan@ymail.com</a></p>
                    <div class="d-flex justify-content-start mt-4">
                        <a class="btn btn-outline-light btn-square mr-2" href="#"><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-outline-light btn-square mr-2" href="#"><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-outline-light btn-square mr-2" href="#"><i class="fab fa-linkedin-in"></i></a>
                        <a class="btn btn-outline-light btn-square" href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
                <div class="col-md-6 mb-5">
                    <h5 class="text-primary text-uppercase mb-4" style="letter-spacing: 5px;">Tentang Kami</h5>
                    <div class="d-flex flex-column justify-content-start">
                        <a class="text-white mb-2" href="about"><i class="fa fa-angle-right mr-2"></i>Tentang Kami</a>
                        <a class="text-white mb-2" href="contact"><i class="fa fa-angle-right mr-2"></i>Hubungi Kami</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-5 col-md-12 mb-5">
            <h5 class="text-primary text-uppercase mb-4" style="letter-spacing: 5px;">Langgan Promosi</h5>
            <div class="w-100">
                <div class="input-group">
                    <input type="text" class="form-control border-light" style="padding: 30px;" placeholder="Alamat Emel Anda">
                    <div class="input-group-append">
                        <button class="btn btn-primary px-4">Langgan Sekarang</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer End -->