<!-- Topbar Start -->
<div class="container-fluid d-none d-lg-block">
    <div class="row align-items-center py-3 px-xl-5">
        <div class="col-lg-5">
            <a href="" class="text-decoration-none">
                <h1 class="m-0"><span class="text-primary">JUTAWAN</span>ELEGANCE</h1>
            </a>
        </div>
        <div class="col-lg-3 text-right">
            <div class="d-inline-flex align-items-center">
                <i class="fa fa-2x fa-map-marker-alt text-primary mr-3"></i>
                <div class="text-left">
                    <h6 class="font-weight-semi-bold mb-1">Pejabat</h6>
                    <small><a style="text-decoration: none; color: black;" href="https://goo.gl/maps/osow6fYbWA9X7qvAA" target="_blank">
                            42-1, Jalan Komersil Senawang 7, Taman Komersil Senawang, 70450 Seremban, Negeri Sembilan</a>
                    </small>
                </div>
            </div>
        </div>
        <div class="col-lg-2 text-right">
            <div class="d-inline-flex align-items-center">
                <i class="fa fa-2x fa-envelope text-primary mr-3"></i>
                <div class="text-left">
                    <h6 class="font-weight-semi-bold mb-1">Emel</h6>
                    <small><a style="text-decoration: none; color: black;" href="mailto: azizigandan@ymail.com">azizigandan@ymail.com</a></small>
                </div>
            </div>
        </div>
        <div class="col-lg-2 text-right">
            <div class="d-inline-flex align-items-center">
                <i class="fa fa-2x fa-phone text-primary mr-3"></i>
                <div class="text-left">
                    <h6 class="font-weight-semi-bold mb-1">No. Telefon</h6>
                    <small><a style="text-decoration: none; color: black;" href="tel: 0196388448">+019 638 8448</a></small>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Topbar End -->