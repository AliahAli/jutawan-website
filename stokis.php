<!DOCTYPE html>
<html lang="en">

<?php include 'includes/header.php' ?>


<body>
    <?php include 'includes/topbar.php' ?>

    <?php include 'includes/navigation.php' ?>

    <!-- Header Start -->
    <div class="container-fluid page-header" style="margin-bottom: 90px;">
        <div class="container">
            <div class="d-flex flex-column justify-content-center" style="min-height: 300px">
                <h3 class="display-4 text-white text-uppercase">Stokis</h3>
                <div class="d-inline-flex text-white">
                    <p class="m-0 text-uppercase"><a class="text-white" href="">Home</a></p>
                    <i class="fa fa-angle-double-right pt-1 px-3"></i>
                    <p class="m-0 text-uppercase">Stokis</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Header End -->

    <!-- Team Start -->
    <div class="container-fluid py-5">
        <div class="container pt-5 pb-3">
            <div class="text-center mb-5">
                <h5 class="text-primary text-uppercase mb-3" style="letter-spacing: 5px;">Stokis</h5>
                <h1>Senarai Stokis</h1>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-4  team mb-4">
                    <div class="team-item rounded overflow-hidden mb-2">
                        <div class="team-img position-relative text-center">
                            <img class="img-fluid" src="img/team-1.jpg" alt="">
                        </div>
                        <div class="bg-secondary p-4">
                            <h5 class="text-center">Elegance Group</h5>
                            <p class="text-center m-0">Selangor</p>
                            <div class="mt-2">
                                <p class="m-0"><i class="fa  fa-envelope text-primary mr-3"></i>elegancegroup2016@gmail.com</p>
                                <p class="m-0"><i class="fa  fa-phone text-primary mr-3"></i>0189810103</p>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4  team mb-4">
                    <div class="team-item rounded overflow-hidden mb-2">
                        <div class="team-img position-relative text-center">
                            <img class="img-fluid" src="img/team-2.jpg" alt="">
                        </div>
                        <div class="bg-secondary p-4">
                            <h5 class="text-center">Sam</h5>
                            <p class="text-center m-0">Selangor</p>
                            <div class="mt-2">
                                <p class="m-0"><i class="fa  fa-envelope text-primary mr-3"></i>samrohani5513@gmail.com</p>
                                <p class="m-0"><i class="fa  fa-phone text-primary mr-3"></i>01052238373</p>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4  team mb-4">
                    <div class="team-item rounded overflow-hidden mb-2">
                        <div class="team-img position-relative text-center">
                            <img class="img-fluid" src="img/team-3.jpg" alt="">
                        </div>
                        <div class="bg-secondary p-4">
                            <h5 class="text-center">Datin Nor Asikin Binti Taib</h5>
                            <p class="text-center m-0">Negeri Sembilan</p>
                            <div class="mt-2">
                                <p class="m-0"><i class="fa  fa-envelope text-primary mr-3"></i>prousahawann9@gmail.com</p>
                                <p class="m-0"><i class="fa  fa-phone text-primary mr-3"></i>01125543454</p>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4  team mb-4">
                    <div class="team-item rounded overflow-hidden mb-2">
                        <div class="team-img position-relative text-center">
                            <img class="img-fluid" src="img/team-4.jpg" alt="">
                        </div>
                        <div class="bg-secondary p-4">
                            <h5 class="text-center">Norrasid Bin Ramli </h5>
                            <p class="text-center m-0">Negeri Sembilan</p>
                            <div class="mt-2">
                                <p class="m-0"><i class="fa  fa-envelope text-primary mr-3"></i>rasmah7374@gmail.com</p>
                                <p class="m-0"><i class="fa  fa-phone text-primary mr-3"></i>0137427374</p>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4  team mb-4">
                    <div class="team-item rounded overflow-hidden mb-2">
                        <div class="team-img position-relative text-center">
                            <img class="img-fluid" src="img/team-1.jpg" alt="">
                        </div>
                        <div class="bg-secondary p-4">
                            <h5 class="text-center">Hasni Binti Nayan</h5>
                            <p class="text-center m-0">Negeri Sembilan</p>
                            <div class="mt-2">
                                <p class="m-0"><i class="fa  fa-envelope text-primary mr-3"></i>hasni1966@gmail.com</p>
                                <p class="m-0"><i class="fa  fa-phone text-primary mr-3"></i>0193127913</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Team End -->



    <?php include 'includes/footer.php' ?>

    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="fa fa-angle-double-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>

    <!-- Contact Javascript File -->
    <script src="mail/jqBootstrapValidation.min.js"></script>
    <script src="mail/contact.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
</body>

</html>