<!DOCTYPE html>
<html lang="en">

<?php include 'includes/header.php' ?>

<body>
    <?php include 'includes/topbar.php' ?>

    <?php include 'includes/navigation.php' ?>

    <!-- Header Start -->
    <div class="container-fluid page-header" style="margin-bottom: 90px;">
        <div class="container">
            <div class="d-flex flex-column justify-content-center" style="min-height: 300px">
                <h3 class="display-4 text-white text-uppercase">Produk</h3>
                <div class="d-inline-flex text-white">
                    <p class="m-0 text-uppercase"><a class="text-white" href="">Home</a></p>
                    <i class="fa fa-angle-double-right pt-1 px-3"></i>
                    <p class="m-0 text-uppercase">Produk</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Header End -->

    <!-- Carousel Category Start -->
    <div class="container-fluid mb-3">
        <div class="text-center mb-5">
            <h5 class="text-primary text-uppercase mb-3" style="letter-spacing: 5px;">Kategori Produk</h5>
        </div>
        <div class="row px-xl-5 justify-content-center">
            <div class="col-lg-8">
                <div id="header-carousel" class="carousel slide carousel-fade mb-30 mb-lg-0 justify-content-center" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#header-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#header-carousel" data-slide-to="1"></li>
                        <li data-target="#header-carousel" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item position-relative active" style="height: 430px;">
                            <img class="position-absolute w-100 h-100" src="img/category-1.jpg" style="object-fit: cover;">
                            <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                                <div class="p-3 align-items-center justify-center" style="max-width: 700px;">
                                    <h1 class="display-4 text-white mb-3 animate__animated animate__fadeInDown">Produk Kesihatan</h1>
                                    <p class="mb-md-4 animate__animated animate__bounceIn">Lorem rebum magna amet lorem magna erat diam stet. Sadips duo stet amet amet ndiam elitr ipsum diam</p>
                                    <a href="" class="btn btn-outline-light py-md-2 px-md-4 mt-2 animate__animated animate__fadeInUp">Beli Sekarang</a>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item position-relative" style="height: 430px;">
                            <img class="position-absolute w-100 h-100" src="img/carousel-7.jpg" style="object-fit: cover;">
                            <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                                <div class="p-3 align-items-center justify-center" style="max-width: 700px;">
                                    <h1 class="display-4 text-white mb-3 animate__animated animate__fadeInDown">Produk Kecantikan</h1>
                                    <p class="mb-md-4 animate__animated animate__bounceIn">Lorem rebum magna amet lorem magna erat diam stet. Sadips duo stet amet amet ndiam elitr ipsum diam</p>
                                    <a href="" class="btn btn-outline-light py-md-2 px-md-4 mt-2 animate__animated animate__fadeInUp">Beli Sekarang</a>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item position-relative" style="height: 430px;">
                            <img class="position-absolute w-100 h-100" src="img/category-3.jpg" style="object-fit: cover;">
                            <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                                <div class="p-3 align-items-center justify-center" style="max-width: 700px;">
                                    <h1 class="display-4 text-white mb-3 animate__animated animate__fadeInDown">Produk Kesihatan Lelaki</h1>
                                    <p class="mb-md-4 animate__animated animate__bounceIn">Lorem rebum magna amet lorem magna erat diam stet. Sadips duo stet amet amet ndiam elitr ipsum diam</p>
                                    <a href="" class="btn btn-outline-light py-md-2 px-md-4 mt-2 animate__animated animate__fadeInUp">Beli Sekarang</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Carousel Category End -->

    <!-- Product Start -->
    <div class="container-fluid py-5">
        <div class="container py-5">
            <div class="text-center mb-5">
                <h5 class="text-primary text-uppercase mb-3" style="letter-spacing: 5px;">Produk</h5>
                <h1>Katalog Produk Kami</h1>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/product/himayah-qufit.png" alt="">
                        <div class="bg-secondary p-4">
                            <a class="h5" href="">Himayah Qufit</a>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h5 class="m-0">RM 80</h5>
                                    <div class="d-inline-block">
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/product/prelove.png" alt="">
                        <div class="bg-secondary p-4">
                            <a class="h5 mb-1" href="">Himayah Prelove</a>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h5 class="m-0">RM 70</h5>
                                    <div class="d-inline-block">
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/product/cocoa-drink.png" alt="">
                        <div class="bg-secondary p-4">
                            <a class="h5 mb-1" href="">Oredi Premium Cocoa Drink</a>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h5 class="m-0">RM 70</h5>
                                    <div class="d-inline-block">
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/product/himayah-qufit.png" alt="">
                        <div class="bg-secondary p-4">
                            <a class="h5" href="">Himayah Coffee</a>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h5 class="m-0">RM 90</h5>
                                    <div class="d-inline-block">
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/product/lemongrass.png" alt="">
                        <div class="bg-secondary p-4">
                            <a class="h5" href="">Himayah Lemongrass</a>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h5 class="m-0">RM 80</h5>
                                    <div class="d-inline-block">
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/product/susu-kunyit.png" alt="">
                        <div class="bg-secondary p-4">
                            <a class="h5 mb-1" href="">Himayah Susu Kunyit</a>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h5 class="m-0">RM 70</h5>
                                    <div class="d-inline-block">
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/product/himayah-haircare.png" alt="">
                        <div class="bg-secondary p-4">
                            <a class="h5 mb-1" href="">Himayah Hair Care</a>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h5 class="m-0">RM 70</h5>
                                    <div class="d-inline-block">
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/product/lipstick.png" alt="">
                        <div class="bg-secondary p-4">
                            <a class="h5 mb-1" href="">Himayah Lipstick</a>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h5 class="m-0">RM 70</h5>
                                    <div class="d-inline-block">
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/product/foundation.png" alt="">
                        <div class="bg-secondary p-4">
                            <a class="h5 mb-1" href="">Himayah Foundation</a>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h5 class="m-0">RM 70</h5>
                                    <div class="d-inline-block">
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/product/eczema.png" alt="">
                        <div class="bg-secondary p-4">
                            <a class="h5" href="">Eczema Relief Cream</a>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h5 class="m-0">RM 90</h5>
                                    <div class="d-inline-block">
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/product/herys.png" alt="">
                        <div class="bg-secondary p-4">
                            <a class="h5 mb-1" href="">Herys Lotion</a>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h5 class="m-0">RM 70</h5>
                                    <div class="d-inline-block">
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/product/hair-serum.png" alt="">
                        <div class="bg-secondary p-4">
                            <a class="h5 mb-1" href="">Elegance Daddy Hair Serum</a>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h5 class="m-0">RM 70</h5>
                                    <div class="d-inline-block">
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/product/hair-pomade.png" alt="">
                        <div class="bg-secondary p-4">
                            <a class="h5 mb-1" href="">Elegance Daddy Hair Pomade</a>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h5 class="m-0">RM 70</h5>
                                    <div class="d-inline-block">
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/product/men-healthcare.png" alt="">
                        <div class="bg-secondary p-4">
                            <a class="h5 mb-1" href="">Men Health Care</a>
                            <div>
                                <h7>Minuman Campuran</h7>
                            </div>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h5 class="m-0">RM 120</h5>
                                    <div class="d-inline-block">
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/product/men-bodycare.png" alt="">
                        <div class="bg-secondary p-4">
                            <a class="h5 mb-1" href="">Men Body Care</a>
                            <div>
                                <h7>Tablet Super Power</h7>
                            </div>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h5 class="m-0">RM 120</h5>
                                    <div class="d-inline-block">
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/product/men-bodycare.png" alt="">
                        <div class="bg-secondary p-4">
                            <a class="h5 mb-1" href="">Men Body Care</a>
                            <div>
                                <h7>Cream Super Power</h7>
                            </div>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h5 class="m-0">RM 80</h5>
                                    <div class="d-inline-block">
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/product/Product-14.jpeg" alt="">
                        <div class="bg-secondary p-4">
                            <a class="h5" href="">Tobek Hair Serum</a>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h5 class="m-0">RM 70</h5>
                                    <div class="d-inline-block">
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/product/Product-7.jpeg" alt="">
                        <div class="bg-secondary p-4">
                            <a class="h5" href="">Tobek Hair Pomade</a>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h5 class="m-0">RM 120</h5>
                                    <div class="d-inline-block">
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Product End -->

    <?php include 'includes/footer.php' ?>


    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="fa fa-angle-double-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>

    <!-- Contact Javascript File -->
    <script src="mail/jqBootstrapValidation.min.js"></script>
    <script src="mail/contact.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
</body>

</html>