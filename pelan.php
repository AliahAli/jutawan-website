<!DOCTYPE html>
<html lang="en">

<?php include 'includes/header.php' ?>

<body>
    <?php include 'includes/topbar.php' ?>

    <?php include 'includes/navigation.php' ?>

    <!-- Header Start -->
    <div class="container-fluid page-header" style="margin-bottom: 90px;">
        <div class="container">
            <div class="d-flex flex-column justify-content-center" style="min-height: 300px">
                <h3 class="display-4 text-white text-uppercase">PELAN</h3>
                <div class="d-inline-flex text-white">
                    <p class="m-0 text-uppercase"><a class="text-white" href="">Home</a></p>
                    <i class="fa fa-angle-double-right pt-1 px-3"></i>
                    <p class="m-0 text-uppercase">PELAN</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Header End -->

    <!-- Carousel Category Start -->
    <div class="container-fluid mb-3">
        <div class="text-center mb-5">
            <h5 class="text-primary text-uppercase mb-3" style="letter-spacing: 5px;">Selamat Datang ke JUTAWANELEGANCE4U</h5>
            <h1>PELUANG PERNIAGAAN</h1>
        </div>
        <div class="row px-xl-5 justify-content-center">
            <div class="col-lg-7 mb-5 ">
                <div id="header-carousel" class="carousel slide carousel-fade mb-30 mb-lg-0 justify-content-center" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#header-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#header-carousel" data-slide-to="1"></li>
                        <li data-target="#header-carousel" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item position-relative active" style="height: 480px;">
                            <img class="position-absolute w-100 h-100" src="img/pelan-1.jpg" style="object-fit: cover;">
                            <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                                <div class="p-3 align-items-center justify-center" style="max-width: 700px;">
                                    <h1 class="display-4 text-white mb-3 animate__animated animate__fadeInDown">Keistimewaan Ahli</h1>
                                    <p class="mb-md-4 animate__animated animate__bounceIn">1. Keahlian seumur hidup</p>
                                    <p class="mb-md-4 animate__animated animate__bounceIn">2. Auto maintanance 10%</p>
                                    <p class="mb-md-4 animate__animated animate__bounceIn">3. Untung runcit tinggi 10%-30%</p>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item position-relative" style="height: 480px;">
                            <img class="position-absolute w-100 h-100" src="img/pelan-2.jpg" style="object-fit: cover;">
                            <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                                <div class="p-3 align-items-center justify-center" style="max-width: 700px;">
                                    <h1 class="display-4 text-white mb-3 animate__animated animate__fadeInDown">Keistimewaan Ahli</h1>
                                    <p class="text-white">1. Komisen Harian</p>
                                    <ul class="list-inline text-white">
                                        <li><i class="fa fa-check text-primary mr-3"></i>Mobil stokis 5%</li>
                                        <li><i class="fa fa-check text-primary mr-3"></i>Affiliate unilevel 40%</li>
                                    </ul>
                                    <p class="text-white">2. Komisen Bulanan</p>
                                    <ul class="list-inline text-white">
                                        <li><i class="fa fa-check text-primary mr-3"></i>Affiliate unilevel maintain 40%</li>
                                    </ul>
                                    <p class="text-white">3. Bonus leadership 3%</p>
                                    <ul class="list-inline text-white">
                                        <li><i class="fa fa-check text-primary mr-3"></i>CTO 2%</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item position-relative" style="height: 480px;">
                            <img class="position-absolute w-100 h-100" src="img/pelan-3.jpg" style="object-fit: cover;">
                            <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                                <div class="p-3 align-items-center justify-center" style="max-width: 700px;">
                                    <h1 class="display-4 text-white mb-3 animate__animated animate__fadeInDown">Keistimewaan Ahli</h1>
                                    <p class="text-white text-center">PERCUMA PA INSURANS</p>
                                    <p class="text-white">1. Pampasan kematian akibat kemalangan = RM 5,000</p>
                                    <p class="text-white">2. Khairat kematian kemalangan = RM 1,000</p>
                                    <p class="text-white">3. Kehilangan keupayaan kekal JD II = RM 5000</p>
                                    <p class="text-white">4. Kos perubatan akibat kemalangan = RM 250</p>
                                    <p class="text-white">5. Jumlah keseluruhan = RM 11,250</p>
                                    <p class="text-white ">PELBAGAI GANJARAN</p>
                                    <ul class="list-inline text-whiter">
                                        <li><i class="fa fa-check text-primary mr-3 "></i>Rumah</li>
                                        <li><i class="fa fa-check text-primary mr-3 "></i>Kenderaan</li>
                                        <li><i class="fa fa-check text-primary mr-3 "></i>Pelancongan</li>
                                        <li><i class="fa fa-check text-primary mr-3 "></i>Penganugerahan</li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Carousel Category End -->

    <div class="container-fluid">
        <div class="container py-5">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <img class="img-fluid rounded mb-4 mb-lg-0" src="img/pelan-1.png" alt="">
                </div>
                <div class="col-lg-7">
                    <div class="text-left mb-4">
                        <h1>Untung Runcit</h1>
                    </div>
                    <p>1. Setiap pembelian satu produk ada harga jualan/<strong>untung runcit</strong> secara tunai</p>
                    <p>2. Anda daftar nama pelanggan atau nama sendiri anda perolehi <strong>Bonus Affiliate</strong> 20%</p>
                    <p>Contoh :</p>
                    <p><strong>Himayah Qufit :</strong></p>
                    <ul>
                        <li>Harga ahli = RM50 (1 pin pendaftaran)</li>
                        <li>Harga jual = <u>RM70</u></li>
                        <li>Untung runcit = RM20 tunai langsung</li>
                        <li>Daftar ahli = <u>RM10</u> Tunai wallet</li>
                        <li>Jumlah Untung = RM30</li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid ">
        <div class="container py-5">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <img class="img-fluid rounded mb-4 mb-lg-0" src="img/mobil-1.jpg" alt="">
                </div>
                <div class="col-lg-7">
                    <div class="text-left mb-4">
                        <h1>Mobil Stokis 5%</h1>
                    </div>
                    <ul>
                        <li>Anda Harus Berdaftar Pakej DGL = RM 1,000</li>
                        <li>Belian Stock Mobil Stokis = RM 3,000 + Kos Penghantaran</li>
                    </ul>
                    <p>1. Syarat-syarat tetap belian produk adalah seperti ikut carta dibawah</p>
                    <p>2. Jika anda memilih satu atau 2 jenis produk pakej Mobil Stokis anda harus membeli dengan jumlah kiraan 60 kotak @ 60 Pin pendaftaran samada melalui Post atau Ambil sendiri</p>
                    <p>3. Kos penghantaran jika anda mahu menanggungnya sendiri juga boleh, produk sampai anda harus bayar kos penghantaran</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Detail Start -->
    <div class="container-fluid py-5">
        <div class="container py-5">
            <div class="row">
                <div class="col-lg-8">
                    <div class="mb-5">
                        <h1 class="mb-5">Belian produk</h1>
                        <img class="img-fluid rounded w-100 mb-4" src="img/belian-1.jpg" alt="Image">
                        <p><strong>Usahawan atau pengguna sila ikuti cara belian online banking</strong></p>
                        <p>Pembelian produk maintan RM100 atau belian peribadi maklumat yang diperlukan:</p>
                        <ul>
                            <li>Username Peribadi:</li>
                            <li>Jenis Produk:</li>
                            <li>Jumlahh Belian:</li>
                        </ul>
                        <p>Pembelian pakej pin pendaftaran</p>
                        <ul>
                            <li>Username pengenal : untuk di transfer pin pendaftaran</li>
                            <li>Pastikan ahli daftar menggunakan link percuma dahulu kemudian pengenal aktifkan</li>
                        </ul>
                        <p><strong>Pendaftaran online</strong></p>
                        <p>Cara Belian/Pendaftaran :-</p>
                        <ul>
                            <p>1. Buat pilihan pakej dan produk di post atau ambil di HQ</p>
                            <li>Bayaran secara<strong>Online Banking(FPX Online Transfer)</strong></li>
                            <li>Melalui <strong>Mobil Stokis</strong> atau <strong>HQ</strong></li>
                            <li>Apabila daftar pakej RM50 anda tidak boleh upgrade lompat ke pakej besar</li>
                        </ul>
                        <ul>
                            <p>2. Syarat Mobil Stokis</p>
                            <li>Harus daftar pakej <strong>DGL = RM 1,000</strong></strong></li>
                            <li>Belian <strong>Stock Produk</strong> + <strong>Pin Pendaftaran</strong> = RM 3,000(60 kotak + 60 Pin)</li>
                        </ul>
                    </div>

                </div>

                <div class="col-lg-3 mt-6 mt-lg-0">
                    <!-- Author Bio -->
                    <div class="d-flex flex-column text-center bg-dark rounded mb-5 py-5 px-9">
                        <img src="img/belian-2.png" class="img-fluid rounded-circle mx-auto mb-3" style="width: 100px;">
                        <h3 class="text-primary mb-3">8010630799</h3>
                        <p class="text-white m-0">Penghantaran</p>
                        <li class="text-white">Post</li>
                        <li class="text-white">Ambil sendiri</li>
                        <div class="mt-1">
                            <p class="text-white">Ingatan kepada semua. Setiap pembelian diatas disertakan slip bank pengesahan pembelian. Sila whatsapp kepada Sales Marketing untuk rujukan</p>
                        </div>
                        <div class="mt-1">
                            <p class="text-white">Hotline Sales Marketing</p>
                            <p class="text-white">Waktu Bekerja : <br> 0900 Pagi - 0600 Petang</p>
                            <p class="text-white">(0125490198 / 0132108088)</p>
                            <p class="text-white">Lepas Kerja : <br> 0600 Petang - 0900 Malam</p>
                            <p class="text-white">(0105228378 / 0132108088)</p>
                        </div>
                    </div>

                </div>
                <div class="row px-xl-5 justify-content-center">
                    <div class="col-lg-7 mb-5 ">
                        <img src="img/belian-3.png" class="img-fluid rounded w-100 mb-4" style="width: 100px;">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Detail End -->

    <!-- Detail Start -->
    <div class="container-fluid ">
        <div class="container py-5">
            <div class="text-center mb-5">
                <h1>Affiliate Unilevel Maintain 40%</h1>
            </div>
            <div class="text-center">
                <p><strong>10 % Auto Maintain bulanan adalah untuk upgrade/maintain unilevel adalah manual</strong></p>
            </div>
            <p>Pecahan 10% Auto Maintain :-</p>
            <p>5% - Simpanan bulanan boleh digunakan sebagai upgrade pangkat/maintain unilevel</p>
            <ul>
                <li>Maintain semua pangkat RM100 min/max</li>
                <li>Sekiranya tidak cukup boleh topup EP disyorkan</li>
                <li>Tempoh bermula maintain 1 bulan 7 hari</li>
                <li> Contoh Jan 30 hari + Feb 7 hari</li>
                <li>Jika tidak maintain dalam masa 37 hari bonus tidak dapat dikeluarkan.</li>
                <li>Tempoh maintain sehingga 12 bulan bonus akan hilang selepas 12 bulan</li>
                <li>3% Affiliate Leadership Pool, setiap bulan dikongsikan kepada <strong>DSL & DGL</strong> yang layak (Wajib melahirkan 5 usahawan DSL dan DGL setiap bulan)</li>
                <li><strong>CTO 2% - DGL</strong> berjaya mendaftar seramai 100 ahli dalam masa setahun akan dikongsi sama rata</li>
            </ul>
            <div class="container pt-5 pb-3">
                <div class="row">
                    <div class="col-lg-6 col-md-8 mb-8">
                        <img src="img/afi-1.jpg" class="img-fluid rounded w-100 mb-4" style="width: 100px;">

                    </div>
                    <div class="col-lg-6 col-md-8 mb-8">
                        <img src="img/afi-2.jpg" class="img-fluid rounded w-100 mb-4" style="width: 100px;">

                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Detail End -->

    <?php include 'includes/footer.php' ?>

    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="fa fa-angle-double-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>

    <!-- Contact Javascript File -->
    <script src="mail/jqBootstrapValidation.min.js"></script>
    <script src="mail/contact.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
</body>

</html>