<!DOCTYPE html>
<html lang="en">

<?php include 'includes/header.php' ?>

<?php

use PHPMailer\PHPMailer\PHPMailer;

$result = "";
if (isset($_POST['submit'])) {
    require 'vendor/autoload.php';
    $setting = require 'includes/email.php';
    $mail = new PHPMailer(true);
    $mail->isSMTP();                     //Set the SMTP server to send through
    $mail->Host       = $setting['host'];                     //Set the SMTP server to send through
    $mail->Port       = $setting['port'];               //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;            //Enable implicit TLS encryption
    $mail->Username   = $setting['username'];                     //SMTP username
    $mail->Password   = $setting['password'];                     //SMTP password

    //Recipients
    $mail->setFrom($setting['from'], $setting['from_name']);
    $mail->addAddress($setting['to']);     //Add a recipient

    //Content
    $mail->isHTML(true);
    $mail->Subject = 'Form Submission:' . $_POST['subject'];
    $mail->Body    = '<h1 align=center>Name :' . $_POST['name'] . '<br>Email: ' . $_POST['email'] . '<br>Phone: ' . $_POST['phone'] . ' <br>Message: ' . $_POST['message'] . '</h1>';

    if ($mail->send()) {
        $result = '<div class="alert alert-warning" role="alert">
        Email sent!
     </div>';
    } else {
        $result = '<div class="alert alert-warning" role="alert">
        Failed to send 
     </div>';
    }
}

?>


<body>
    <?php include 'includes/topbar.php' ?>

    <?php include 'includes/navigation.php' ?>

    <!-- Header Start -->
    <div class="container-fluid page-header" style="margin-bottom: 90px;">
        <div class="container">
            <div class="d-flex flex-column justify-content-center" style="min-height: 300px">
                <h3 class="display-4 text-white text-uppercase">Hubungi Admin</h3>
                <div class="d-inline-flex text-white">
                    <p class="m-0 text-uppercase"><a class="text-white" href="">Laman Utama</a></p>
                    <i class="fa fa-angle-double-right pt-1 px-3"></i>
                    <p class="m-0 text-uppercase">Hubungi Admin</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Header End -->


    <!-- Contact Start -->
    <div class="container-fluid py-5">
        <div class="container py-5">
            <div class="text-center mb-5">
                <h5 class="text-primary text-uppercase mb-3" style="letter-spacing: 5px;">Hubungi Admin</h5>
                <h1>Hubungi Untuk Sebarang Pertanyaan</h1>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="contact-form bg-secondary rounded p-5">
                        <div id="success"></div>
                        <form method="POST">
                            <h5 class="text-center text-success"><?= $result; ?></h5>
                            <div class="control-group">
                                <input name="name" type="text" class="form-control border-0 p-4" id="name" placeholder="Nama" required="required" data-validation-required-message="Sila masukkan nama anda" />
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="control-group">
                                <input name="email" type="email" class="form-control border-0 p-4" id="email" placeholder="Email" required="required" data-validation-required-message="Sila masukkan email anda" />
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="control-group">
                                <input name="phone" type="phone" class="form-control border-0 p-4" id="phone" placeholder="No. Telefon" required="required" data-validation-required-message="Sila masukkan nombor telefon anda" />
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="control-group">
                                <input name="subject" type="text" class="form-control border-0 p-4" id="phone" placeholder="Subjek" required="required" data-validation-required-message="Sila masukkan nombor telefon anda" />
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="control-group">
                                <textarea name="message" class="form-control border-0 py-3 px-4" rows="5" id="message" placeholder="Pesanan Anda" required="required" data-validation-required-message="Sila masukkan pesanan anda"></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="text-center">
                                <input name="submit" class="btn btn-primary py-3 px-5" type="submit" id="submit" value="Hantar Mesej" ></input>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact End -->


    <?php include 'includes/footer.php' ?>


    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="fa fa-angle-double-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>

    <!-- Contact Javascript File -->
    <script src="mail/jqBootstrapValidation.min.js"></script>
    <script src="mail/contact.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
</body>

</html>
